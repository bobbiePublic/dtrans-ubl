# dtrans-ubl
This repository contains ubl translations used by the 1Lieferschein frontend and apps.
The generator script under _source converts the source files into usable formats required by the components.
To regenerate the files from the sources run the python script in the _source directory.

The generated files can then be found under:
- android
- ios
- web

## Important: DO NOT EDIT THE FILES IN THESE DIRECTORIES DIRECTLY.
They are updated by running the script.
**Only edit** the source files in directory _source if you need to update the translation strings.
Also make sure to commit the generated files after altering the source files as these files are automatically included via packagemangers/scripts on build.

**Current UBL Version:** 2.4
