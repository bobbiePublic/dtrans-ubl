import json
import sys


def write_translation(input: str, android: str, ios: str, web: str):
    # Open and read the JSON file
    with open(input, 'r') as file:
        data = json.load(file)

    xml = {}
    strings = {}
    js = {}
    doc = {}

    for entry_name in data:
        entry = data.get(entry_name)
        if entry_name == '_Attributes':
            js['attributes'] = entry
            for attr in entry:
                xml[attr] = entry.get(attr)
                strings[attr] = entry.get(attr)
            continue

        for c in entry:
            e = entry.get(c)
            xml[c] = e.get('display')

            ef = entry_name[0:-4]
            n = ef + '.' + c
            strings[n] = e.get('display')

            eg = entry_name + '#' + c
            doc[eg] = e

    # write xml
    with open('../android/' + android, 'w') as fp:
        fp.write('''<?xml version="1.0" encoding="utf-8"?>
<resources>
    <!-- TYPES -->\n''')
        for entry_name in xml:
            entry = xml.get(entry_name)
            fp.write('    <string name="' + entry_name + '">' + entry + '</string>\n')

        fp.write('''</resources>\n''')

    # write ios
    with open('../ios/' + ios, 'w') as fp:
        for s in strings:
            se = strings.get(s)
            fp.write('"' + s + '" = "' + se + '";\n')


    # write web
    js['documentation'] = doc
    with open('../web/' + web, 'w') as fp:
        json.dump(js, fp, indent=4, ensure_ascii=False)


write_translation('source_en.json', 'strings_english.xml', 'english.strings', 'doc_english.json')
write_translation('source_de.json', 'strings_german.xml', 'german.strings', 'doc_german.json')
