# this simple scripts converts the web format back to the source format
# useful for new strings like new types from newer UBL versions
import json
import sys

input = sys.argv[1]

# Open and read the JSON file
with open(input, 'r') as file:
    data = json.load(file)

doc = data['documentation']
attr = data['attributes']

ublTypes = {}

for entry_name in doc:
    entry = doc.get(entry_name)
    examples = entry.get('examples')
    if examples != None:
        entry['examples'] =  ', '.join(examples)

    l = entry_name.split('#')
    parentType = l[0]
    childType = l[1]

    superType = ublTypes.get(parentType)
    if superType == None:
        superType = {}

    superType[childType] = entry
    ublTypes[parentType] = superType

with open(input + '_result.json', 'w') as fp:
    json.dump(ublTypes, fp, indent=4, ensure_ascii=False)
